import config
from SetupWindow import SetupWindow
import winreg


if __name__== "__main__":
    test = config.SyncPoints()
    if not test.CheckExists("YOADDC-AG500123"):
       test.AddSyncPoint("YOADDC-AG500123", "\\YOADDC-AG500123\Sync", "Bi-Directional", 0)
    if not test.CheckExists("YOADDC-AG123123"):
        test.AddSyncPoint("YOADDC-AG123123", "\\YOADDC-AG123123\Sync", "Bi-Directional", 0)
    if not test.CheckExists("YOADDC-AG123123"):
        test.AddSyncPoint("YOADDC-AG123123", "\\YOADDC-AG123123\Sync", "Bi-Directional", 0)
    print(test.DisplaySyncPoints())

    DBConfig = config.DBInfo()
    # Lets check if the software has been setup!
    while True:
        try:
            key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "Software\\DBSync")
            value, regType = winreg.QueryValueEx(key, "Version")
            print("Gotcha")
        except FileNotFoundError:
            print("Registry entry was not found")
            RunSetup = SetupWindow()
            RunSetup.mainloop()
            break
        except Excpetion as ex:
            print(str(ex))
        #finally:
         #   print(value)
