# Created by Tim Neufeld
# Date: Sept 2, 2018
# Support email: tim.neufeld@ddcorp.ca

# This software was designed to sync some files to three different USB hard drives over the network.  
# It saves the config file to the users appdata directory along with a log file.
# The software will also self setup.  If it does not see the config file, then it goes into setup mode.
#   In this mode it will create the config.ini and copy the exe file to \ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp.

import sys
import os
from datetime import datetime
from time import sleep, time


mydir = []
myfiles = []
remoteFiles = []
excludeDirs = ("$RECYCLE.BIN", ".bzvol")

# Global variables
appdata = os.getenv("appdata")

def setup():

    # This is for the first time run of the program or if the config file is lost.
    dirLists = os.listdir(appdata)
 
    if "DB_DriveSync" not in dirLists:
        os.mkdir(os.path.join(appdata + "\\DB_DriveSync"))
    with open(os.path.join(appdata + "\\DB_DriveSync", "config.ini"), "w+") as f:
        print("Enter all the Paths you want to sync this PC too.  They should be in the format "+
              "C:\Temp,C:\Temp2,C:\Temp3 and so on.")
        ans = input("What is the path to sync too: ")
        for a in ans.split(','):
            f.write(a + "\n")
    # WILL NEED TO PLACE A CHECK FOR THE FINAL PRODUCTION .exe FILE IN \PROGRAMDATA\MICROSOFT\WINDOWS\START MENU\PROGRAMS\STARTUP
    # IF IT IS NOT IN THAT PATH THEN WE COPY IT TO THERE.  THIS WILL ALLOW THE USER TO JUST RUN THE FILE AND IT WILL SELF SETUP.

def log(status, msg):
    dt = datetime.now().strftime("%Y/%b/%d - %H:%M:%S")
    if status.lower() == "error":
        message = "{0}:  --- {1} ---  {2}\n".format(dt, status.upper(), msg)
    else:
        message = "{0}:  {1}\n".format(dt, msg)
    logFile = datetime.today().strftime("%Y-%m-%d") + ".log"
    with open(os.path.join(appdata + "\\DB_DriveSync", logFile), "a+") as f:
        f.write(message)

if __name__ == "__main__":
    print("Searching for config.ini ...")
    #First check to see if there is a config file and if not, run setup.
    appdata = os.getenv("appdata") # Users appdata directory
    if not os.path.isfile(os.path.join(appdata + "\\DB_DriveSync", "config.ini")):
        print("config.ini file not found.  Running Setup...")
        setup()
    # Now we open the config file and read the Paths we are syncing too.
    try:        
        with open(os.path.join(appdata + "\\DB_DriveSync", "config.ini"), "r") as f:
            print("Getting the paths...")
            syncPaths = f.readlines()
    except:
        print("Error")
        sys.exit(0)

    # Next we verify we can access the paths.  If the path is not found.
    # We log the error and remove it from the list. 
    for p in syncPaths:
        if not os.path.isdir(p):        
            log("error", (p + " NOT FOUND!"+
                          " Sync will not happen tp this path..."))
            syncPaths.remove(p)
    print(syncPaths)
    sleep(500)

    # Now we populate the remoteFiles list with all the remote files that should be synced with this PC.

    tempfiles = []
    for p in syncPaths:
        for root, dirs,files in os.walk(p):
            if os.path.isfile(files):
                tempfiles.append = os.path.join(dirs, files)
        remoteFiles = tempfiles
        tempfiles.clear()
    print(remoteFiles)
