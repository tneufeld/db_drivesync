

from tkinter import *
from tkinter.ttk import Label as Label
import tkinter.font as tkfont
from tkinter import messagebox as msgbox
from tkinter import ttk
from threading import Timer



class DBSyncRoot(Tk):
    def __init__(self):
        defaultFont = "Arial 10 bold"
        defaultBackColor = "Black"
        defaultForeColor = "Sky Blue"
        defaultPadding = "padx = 5, pady=10"
        super(DBSyncRoot, self).__init__() # Calls the parent __init__.   Found it at https://www.youtube.com/watch?v=VGIK3P9gNkM
        # Setting up the main form.
        self.title("DBSync")
        self.geometry("625x300")
        #self.minsize(605,300)
        #self.maxsize(605,300)
        self.resizable(width=0, height =0)
        self.font = defaultFont
        self.wm_iconbitmap("sync.ico") # https://icons8.com/icon/set/sync/all and converted with https://icoconvert.com
        self.configure(background="#444444")

        style = ttk.Style()
        style.configure("TCombobox", background="#EEEEEE", foreground="#0000FF", font="Arial 10 bold")
        style.configure("selection.TLabel", background="#444444", foreground = "Sky Blue", font = "Arial 10 bold")
        style.configure("info.TLabel", background="#333333", foreground = "Sky Blue", font = "Arial 10 bold", justify=RIGHT)
        # The dropdown Menu

        self.menuBar = Menu(self)
        self.fileMenu = Menu(self.menuBar, tearoff=0)
        self.helpMenu = Menu(self.menuBar, tearoff=0)
        self.config(menu=self.menuBar)

        self.menuBar.add_cascade(label="File", menu=self.fileMenu) # Add the top "File" menu.
        self.fileMenu.add_command(label="Quit", command=self.destroy)
        
        self.menuBar.add_cascade(label="Help", menu=self.helpMenu) # Add the top "Help" menu.
        self.helpMenu.add_command(label="About")

        # *** Selection Frame ***
        
        #region Selection Frame
        #Setting up the selection frame.
        self.frmSelections = Frame(self, bg="#444444")
        self.frmSelections.place(relwidth = 1, relheight = .15,anchor=NW)

         # The combobox
        #self.lblSyncPoint = Label(self.frmSelections, text = "Sync Points", background="#444444", foreground = "Sky Blue", font = "Arial 10 bold")
        self.lblSyncPoint = Label(self.frmSelections, text = "Sync Points", style="selection.TLabel")
        #self.cmbSyncPoints = ttk.Combobox(self.frmSelections, width = 40, state = "", font = defaultFont)
        self.cmbSyncPoints = ttk.Combobox(self.frmSelections, state = "active", width = 40, font = defaultFont, style="TCombobox")
                # The Add and Delete Sync Point buttons.
        self.btnEditSyncPoint = Button(self.frmSelections, text = "Edit", bg="#004646", width = 10, fg = defaultForeColor, 
                                      font = defaultFont, command=self.addSyncPoints)


        self.btnEditSyncPoint.bind("<Enter>", self.btnEditSyncPoint_MouseEnter)
        self.btnEditSyncPoint.bind("<Leave>", self.btnEditSyncPoint_MouseLeave)

        self.btnDeleteSyncPoint = Button(self.frmSelections, text = "Delete",width = 10, bg = "#860000", 
                                         fg = defaultForeColor, font = defaultFont)
        self.btnDeleteSyncPoint.bind("<Enter>", self.btnDeleteSyncPoint_MouseEnter)
        self.btnDeleteSyncPoint.bind("<Leave>", self.btnDeleteSyncPoint_MouseLeave)

        self.btnEditSyncPoint.grid(row = 0, column = 2, padx = 5, pady=10)
        self.btnDeleteSyncPoint.grid(row=0, column = 3, padx = 5, pady=10)


        self.lblSyncPoint.grid(row=0, column=0, padx = 5, pady=10)
        self.cmbSyncPoints.grid(row = 0, column=1, padx = 5, pady=10)

       # endregion
       # Information Region
       #region Information Frame
        
        self.frmInfo = Frame(self, width = 250, height = 100, bg="#333333")
        self.frmInfo.place(relwidth = .988, relheight = .73, x = 3, y = 48, anchor=NW)
        
        self.lblSyncName = Label(self.frmInfo, text = "SyncPoint Name:", style="info.TLabel")
        txtSyncName = StringVar()
        self.entSyncName = Entry(self.frmInfo, textvariable=txtSyncName, width = 25, background="#EEEEEE", foreground="#111111")
        
        self.lblLocation = Label(self.frmInfo, text = "Sync Location", style="info.TLabel", justify=RIGHT)
        txtLocation = StringVar()
        self.entLocation = Entry(self.frmInfo, textvariable = txtLocation, width = 50, background="#EEEEEE", foreground="#111111")
        
        self.lblSyncType = Label(self.frmInfo, text = "Sync Type", style="info.TLabel")
        self.cmbSyncType = ttk.Combobox(self.frmInfo, width = 25, font = defaultFont, values="Bi-Directional", background="#EEEEEE", foreground="#111111" )
        
        self.lblSyncInterval = Label(self.frmInfo, text = "Sync Interval", style="info.TLabel")
        txtSyncInterval = IntVar()
        self.entSyncInterval = Entry(self.frmInfo, textvariable = txtSyncInterval, width = 10, background="#EEEEEE", foreground="#111111")
        
        self.lblSyncName.grid(row = 0, column = 0)
        self.entSyncName.grid(row = 0, column = 1, sticky=W)
        self.lblLocation.grid(row = 1, column = 0)
        self.entLocation.grid(row = 1, column = 1)
        self.lblSyncType.grid(row = 2, column = 0)
        self.cmbSyncType.grid(row = 2, column = 1, sticky=W)
        self.lblSyncInterval.grid(row = 3, column = 0)
        self.entSyncInterval.grid(row = 3, column = 1, sticky=W)

        # The Add and Delete Sync Point buttons.
        self.btnClearForm = Button(self.frmInfo, text = "Clear", bg="#004646", width = 10, fg = defaultForeColor, 
                                      font = defaultFont, command=self.addSyncPoints)


        self.btnClearForm.bind("<Enter>", self.btnAddSyncPoint_MouseEnter)
        self.btnClearForm.bind("<Leave>", self.btnAddSyncPoint_MouseLeave)

        self.btnApplySyncPoint = Button(self.frmInfo, text = "Apply",bg="#004646", width = 10, fg = defaultForeColor, 
                                      font = defaultFont)
        self.btnApplySyncPoint.bind("<Enter>", self.btnDeleteSyncPoint_MouseEnter)
        self.btnApplySyncPoint.bind("<Leave>", self.btnDeleteSyncPoint_MouseLeave)

        self.btnClearForm.grid(row = 5, column = 2, padx = 5, pady=10)
        self.btnApplySyncPoint.grid(row=5, column = 3, padx = 5, pady=10)


        #endregion
        # Status Frame
        #region Status Frame
        #004646
        self.frmStatus = Frame(self, height = 50, bg="#99BB00", relief=SUNKEN)
        self.frmStatus.place(relwidth=1, relheight=.1, x=0, y=266, anchor=W, bordermode = OUTSIDE)

        self.lblStatus = Label(self.frmStatus,text="Status", anchor=W, justify=LEFT, padding = (5,0,5,0), relief=SUNKEN)
        self.lblStatus.place(x=2, y=2, relwidth=.993, relheight=.88)

        #endregion
        # REMEMBER BIND WILL BIND EVENTS TO WIDGETS!  HELPFUL FOR COMBO VALUE CHANGES?





    def displayToolTip(self, widget, msg):
        # My own tool tip implimentation because I don't think tkinter has one.
        lblToolTip = Label(text=msg, font="Arial -10", state=DISABLED)
        lblToolTip.place(x = widget.winfo_x() + (widget.winfo_width() / 4), 
                         y = widget.winfo_y() + widget.winfo_height())
        t = Timer(2.0, lblToolTip.place_forget)
        t.start()

    def addSyncPoints(self, *val):
        # NEED TO CLEAN THIS UP!
        # First we take existing values in the combobox and add them to tmpList.  We do this because by default,
        # the combobox puts things in tuples.  Then we add the current value to the list, if it is not blank. 
        # Lastly, we add any extra values that were called when the function was called.  Both for 
        # statements should never happen at once but I believe that the function should still handle it nicely.
        tmpList = []
        for n in self.cmbSyncPoints['value']:
            tmpList.append(n)
        if not self.cmbSyncPoints.get() == "":
           tmpList.append(self.cmbSyncPoints.get())
        for n in val:
            tmpList.append(n)
        self.cmbSyncPoints["values"] = tmpList
        
    def btnAddSyncPoint_MouseEnter(self, event):        
        event.widget["foreground"] = "#FFFF00"
        event.widget["font"] = "Arial 10 bold italic"
        self.displayToolTip(self.btnAddSyncPoint, "Add a new sync point!")

    def btnAddSyncPoint_MouseLeave(self, event):
        event.widget["foreground"] = "Sky Blue"
        event.widget["font"] = "Arial 10 bold"

    def btnEditSyncPoint_MouseEnter(self, event):        
        event.widget["foreground"] = "#FFFF00"
        event.widget["font"] = "Arial 10 bold italic"
        self.displayToolTip(self.btnEditSyncPoint, "Edit the current sync point!")

    def btnEditSyncPoint_MouseLeave(self, event):
        event.widget["foreground"] = "Sky Blue"
        event.widget["font"] = "Arial 10 bold"


    def btnDeleteSyncPoint_MouseEnter(self, event):        
        event.widget["foreground"] = "#FFFF00"
        event.widget["font"] = "Arial 10 bold italic"
        self.displayToolTip(self.btnDeleteSyncPoint, "Delete a sync point!")

    def btnDeleteSyncPoint_MouseLeave(self, event):
        event.widget["foreground"] = "Sky Blue"
        event.widget["font"] = "Arial 10 bold"
