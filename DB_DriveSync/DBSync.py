# Creator: Tim Neufeld <tim.neufeld@ddcorp.ca> <tneufeld@gmail.com>
# Project start date: Sept 9, 2018
# GUI: tkinter
# Language: Python 3.6
# Purpose: Sync Files over SMB shares.
# THIS IS WHERE THE PROGRAM WILL START.

import winreg
from DBConfig import DBInfo
import DBui

def checkConfig(dbInfo):
    #dbInfo should be from the DBInfo class in DBConfig library.
    return dbInfo.version


if __name__=="__main__":
    dbInfo = DBInfo() # Setup constant variables.  These never change.
    # Next need to verify installation status.  If reg variables are ** NOT ** 
    # found then activate installation.
    
    while True:
        try:
            regKey = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, dbInfo.RegKey)
            print(str(regKey))
            break
        except Exception as ex:
            print(str(ex))
            break






    #DBSync = DBui.DBSyncRoot()
    #DBSync.mainloop() # This must be the last line.  Once this is done, the program ends.
       
    # Anything past this can be added after the initial form is shut down.
    
    
