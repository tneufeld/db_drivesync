# Created by Tim Neufeld
# Date: Sept 2, 2018
# Support email: tim.neufeld@ddcorp.ca

# This script is for the setup of the software.  The main software looks for the variables in the registry
# and if it does not find them, then it runs this script.  Here all the requirements are checked and if not
# found then it creates them.  This allows the software to be able to self diagnose some minor issues and 
# allow the user to correct if necessary.

from tkinter import *
from config import DBInfo

class SetupWindow(Tk):
    def __init__(self):
        defaultFont = "Arial 10 bold"
        defaultBackColor = "Black"
        defaultForeColor = "Sky Blue"
        defaultPadding = "padx = 5, pady=10"
        super(SetupWindow, self).__init__() # Calls the parent __init__.   Found it at https://www.youtube.com/watch?v=VGIK3P9gNkM
        # Setting up the main form.
        self.title("DBSync Configuration")
        self.geometry("625x300")
        #self.minsize(605,300)
        #self.maxsize(605,300)
        self.resizable(width=0, height =0)
        self.font = defaultFont
        self.wm_iconbitmap("sync.ico") # https://icons8.com/icon/set/sync/all and converted with https://icoconvert.com
        self.configure(background="#444444")

        #Setting up the 

if __name__ == "__main__":

    SetupWin = SetupWindow()
    SetupWin.mainloop()
