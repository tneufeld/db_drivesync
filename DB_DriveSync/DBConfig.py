# Creator: Tim Neufeld
# File: config.py
# Purpose: Handles all the configuration for the program.

class DBInfo():
    '''
    Used to store global information about the program.
    '''
    def __init__(self, *args, **kwargs):
        self.version = "1.0.0"
        self.RegKey = "Software\\DBSync"
        return super().__init__(*args, **kwargs)

class SyncPoints():
    def __init__(self, *args, **kwargs):
        # Store the sync points in a dictionary with the name being the index and the 
        # location, syncType and Interval as parameters.  It will be store in the registery this way as well.
        self.syncPoints = {}
        self._name = ""
        self._location = ""
        self._syncType = ""
        self._syncInterval = 0
        return super().__init__(*args, **kwargs)
    
    def AddSyncPoint(self, syncName, syncLocation, syncType, syncInterval):
        # Add the sync points from either user or registry (During the program load).
        self.syncPoints[syncName] = [syncLocation, syncType, syncInterval]
       
    def DisplaySyncPoints(self):
        # Display the syncPoint when selecting it in the dropdown box.
        return self.syncPoints

    def CheckExists(self, syncName):
        if syncName in self.syncPoints:
            return True
        return False